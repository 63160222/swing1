/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.swing1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author MSI GAMING
 */
public class Frame1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 300);

        JLabel lbHelloWorld = new JLabel("Hello World!",JLabel.CENTER);
        frame.add(lbHelloWorld);
        lbHelloWorld.setBackground(Color.red);
        lbHelloWorld.setOpaque(true);
        lbHelloWorld.setFont(new Font("Verdana",Font.PLAIN,25));

        frame.setVisible(true);

    }
}
